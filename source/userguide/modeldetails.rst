
##################################
Viewing model details
##################################

You can easily deep dive into model architecture, features, lineage graph,
hyperparameters and many more using Scrybe.

.. toctree::
   :maxdepth: 2
   :caption: Model details:



********************************
Navigating to model detail page
********************************
On the experiments tab, click on any model in the table. You should be
navigated to the model detail page of that model. See some of the sample model
detail pages below. Scrybe supports both deep learning and traditional learning
models.


.. image:: ../_static/model_details_1.png
    :width: 1000px
    :height: 600px
    :scale: 50 %
    :alt: alternate text
    :align: center


.. image:: ../_static/model_details_2.png
    :width: 1000px
    :height: 600px
    :scale: 50 %
    :alt: alternate text
    :align: center

