
##################################
Comparing and reviewing models
##################################

You can easily compare and review side by side two models using Scrybe.

.. toctree::
   :maxdepth: 2
   :caption: Comparing models:



*********************
Comparing models
*********************
On the experiments tab, click on the hamburger menu for any two models and
click on Add to Compare as shown below.


.. image:: ../_static/add_to_compare.png
    :width: 1000px
    :height: 600px
    :scale: 50 %
    :alt: alternate text
    :align: center


After that, click on the "Compare" button on the right top of the models table
to jump into the compare view. You should be able to see a side by side
comparison as shown below.


.. image:: ../_static/model_compare.png
    :width: 1000px
    :height: 600px
    :scale: 50 %
    :alt: alternate text
    :align: center



*********************
Benefits
*********************

Model reviews are analogous to code reviews in the traditional software
development world and hold several benefits.

* They help enforce rigour in the model development process early on
* Facilitate collaboration and sharing of knowledge within and across teams
* Increase confidence of stakeholders on the stability of the model.

Model reviews, just like code reviews, are hence a recommended best practice in
the model development life cycle.
