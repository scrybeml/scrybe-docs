.. scrybe documentation master file, created by
   sphinx-quickstart on Fri Nov 22 16:05:51 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#################
Home
#################


.. toctree::
   Home <self>
   :caption: Home
   :hidden:

.. toctree::
   :maxdepth: 4

   gettingstarted
   userguide/introduction
   userguide/modeldetails
   userguide/comparing
   releases
   api
