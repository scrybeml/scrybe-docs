��i      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Tracking with Scrybe�h]�h �Text����Tracking with Scrybe�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�</Users/chandra/scrybe-docs/source/userguide/introduction.rst�hKubh �	paragraph���)��}�(h�jOnce you install the scrybe pip package, it automatically starts uploading all artifacts to the dashboard.�h]�h�jOnce you install the scrybe pip package, it automatically starts uploading all artifacts to the dashboard.�����}�(hh/hh-hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh �compound���)��}�(hhh]��sphinx.addnodes��toctree���)��}�(hhh]�h}�(h]�h!]�h#]�h%]�h']�h�userguide/introduction��entries�]��includefiles�]��maxdepth�K�caption��Basic Tracking:��glob���hidden���includehidden���numbered�K �
titlesonly���
rawentries�]��
rawcaption�hSuh)hAhh*hKhh=ubah}�(h]�h!]��toctree-wrapper�ah#]�h%]�h']�uh)h;hhhhhh*hNubh
)��}�(hhh]�(h)��}�(h�Viewing experiments�h]�h�Viewing experiments�����}�(hhhhhfhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhhchhhh*hKubh,)��}�(h��The experiments tab is a detailed log of your work. By default all experiments
are private, so should be visible only to you.
All your experiment runs
are automatically tracked within the Experiments tabs by default such as in the
image below.�h]�h��The experiments tab is a detailed log of your work. By default all experiments
are private, so should be visible only to you.
All your experiment runs
are automatically tracked within the Experiments tabs by default such as in the
image below.�����}�(hhvhhthhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhchhubh �image���)��}�(h��.. image:: ../_static/Experiments_view_clean.png
    :width: 1000px
    :height: 600px
    :scale: 50 %
    :alt: alternate text
    :align: center

�h]�h}�(h]�h!]�h#]�h%]�h']��width��1000px��height��600px��scale�K2�alt��alternate text��align��center��uri��/userguide/../_static/Experiments_view_clean.png��
candidates�}��*�h�suh)h�hhchhhh*hNubeh}�(h]��viewing-experiments�ah!]�h#]��viewing experiments�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�Viewing plots�h]�h�Viewing plots�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh�hhhh*hK#ubh,)��}�(h�YYour plots should be automatically captured in the plots tab as shown in the image below.�h]�h�YYour plots should be automatically captured in the plots tab as shown in the image below.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK$hh�hhubh�)��}�(h��.. image:: ../_static/plots\ screenshot.png
    :width: 1000px
    :height: 600px
    :scale: 50 %
    :alt: alternate text
    :align: center

�h]�h}�(h]�h!]�h#]�h%]�h']��width��1000px��height��600px��scale�K2�alt��alternate text��align��center��uri��)userguide/../_static/plots screenshot.png�h�}�h�h�suh)h�hh�hhhh*hNubeh}�(h]��viewing-plots�ah!]�h#]��viewing plots�ah%]�h']�uh)h	hhhhhh*hK#ubh
)��}�(hhh]�(h)��}�(h�Viewing datasets�h]�h�Viewing datasets�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh�hhhh*hK0ubh,)��}�(hX  Your datasets should be automatically captured in the datasets tab as shown in the image below.
THe top most dataset is the golden one you load in the beginning, the artifacts
below that are the datasets that were created as a result of modifying the
original dataset.�h]�hX  Your datasets should be automatically captured in the datasets tab as shown in the image below.
THe top most dataset is the golden one you load in the beginning, the artifacts
below that are the datasets that were created as a result of modifying the
original dataset.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK1hh�hhubh�)��}�(h��.. image:: ../_static/plots\ screenshot.png
    :width: 1000px
    :height: 600px
    :scale: 50 %
    :alt: alternate text
    :align: center

�h]�h}�(h]�h!]�h#]�h%]�h']��width��1000px��height��600px��scale�K2�alt��alternate text��align��center��uri��)userguide/../_static/plots screenshot.png�h�}�h�j  suh)h�hh�hhhh*hNubh,)��}�(h��For a deep dive into the datasets, click on the arrow to get information about
all the columns, stats, covariance, correlations stats and plots associated
with the dataset.�h]�h��For a deep dive into the datasets, click on the arrow to get information about
all the columns, stats, covariance, correlations stats and plots associated
with the dataset.�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK>hh�hhubh�)��}�(h��.. image:: ../_static/dataset_page.png
    :width: 1000px
    :height: 600px
    :scale: 50 %
    :alt: alternate text
    :align: center

�h]�h}�(h]�h!]�h#]�h%]�h']��width��1000px��height��600px��scale�K2�alt��alternate text��align��center��uri��%userguide/../_static/dataset_page.png�h�}�h�j8  suh)h�hh�hhhh*hNubh,)��}�(h��To get a lineage of all the code that was used to transform the dataset, click
on the code lineage tab. You should be able to view all the code associated
with transforming the golden dataset to the one you're viewing.�h]�h��To get a lineage of all the code that was used to transform the dataset, click
on the code lineage tab. You should be able to view all the code associated
with transforming the golden dataset to the one you’re viewing.�����}�(hj<  hj:  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKJhh�hhubh�)��}�(h��.. image:: ../_static/dataset_with_code.png
    :width: 1000px
    :height: 600px
    :scale: 50 %
    :alt: alternate text
    :align: center
�h]�h}�(h]�h!]�h#]�h%]�h']��width��1000px��height��600px��scale�K2�alt��alternate text��align��center��uri��*userguide/../_static/dataset_with_code.png�h�}�h�j\  suh)h�hh�hhhh*hNubeh}�(h]��viewing-datasets�ah!]�h#]��viewing datasets�ah%]�h']�uh)h	hhhhhh*hK0ubeh}�(h]��tracking-with-scrybe�ah!]�h#]��tracking with scrybe�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(jk  jh  h�h�h�h�jc  j`  u�	nametypes�}�(jk  Nh�Nh�Njc  Nuh}�(jh  hh�hch�h�j`  h�u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.